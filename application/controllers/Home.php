﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('f_homemodel');
        $this->load->model('f_productmodel');
    }
    //index
    public function lang($lang){
        if($lang!=null){
            $_SESSION['lang']=$lang;
            redirect(base_url());
        }
    }

    public function index($var1= null , $var2 = null){
        if($var1 == null)
        {
            $this->home();
        }else{
            $item = $this->f_homemodel->getFirstRowWhere('alias',array(
                'alias' => $var1
            ));
            if(empty($item)){
                 redirect(base_url('404_override'));
            }
            $item2 = $this->f_homemodel->getFirstRowWhere('alias',array(
                'alias' => $var2
            ));

            if (empty($item2)) {
                $var3 = '';
            }else{
		$var3 = $item2->type;
		if($var2==null){
			$var3 = '';
		}
                
            }

            switch (array($var3,$item->type)) {
                // category
                case array('','cate-pro'):
                    require('Products.php');
                    $index_home = new products();
                    $index_home->pro_bycategory($var1);
                    break;
                case array('','cate-new'):
                    require('News.php');
                    $index_home = new news();
                    $index_home->new_bycategory($var1);
                    break;
                case array('','m-cat'):
                    require('Media.php');
                    $index_home = new media();
                    $index_home->category($var1);
                    break;
                //detail && category
                case array('cate-pro','pro'):
                    require('Products.php');
                    $index_home = new products();
                    $index_home->detail($var1,$var2);
                    break;
                case array('cate-new','new'):
                    require('News.php');
                    $index_home = new news();
                    $index_home->detail($var1,$var2);
                    break;
                case array('m-cat','media'):
                    require('Media.php');
                    $index_home = new media();
                    $index_home->detail($var1,$var2);
                    break;
                case array('','page'):
                    require('Pages.php');
                    $index_home = new pages();
                    $index_home->page_content($var1);
                    break; 

                // detail 

                case array('','pro'):
                    require('Products.php');
                    $index_home = new products();
                    $index_home->detail($var1);
                    break;
                case array('','new'):
                    require('News.php');
                    $index_home = new news();
                    $index_home->detail($var1);
                    break;
                case array('','media'):
                    require('Media.php');
                    $index_home = new media();
                    $index_home->detail($var1);
                    break; 

                // category && category

                case array('cate-pro','cate-pro'):
                    require('Products.php');
                    $index_home = new products();
                    $index_home->pro_bycategory($var1,$var2);
                    break;
                case array('cate-new','cate-new'):
                    require('News.php');
                    $index_home = new news();
                    $index_home->new_bycategory($var1,$var2);
                    break;
                case array('m-cat','m-cat'):
                    require('Media.php');
                    $index_home = new media();
                    $index_home->category($var1,$var2);
                    break;   

            }
        }
    }

    public function home(){ //uh code ci3 k chay dc kieu kia cho nen phai viet lại @@ vẫn chạy đc mà â cac router khac nhu contact , vnadmin co chay dau
        $this->clear_all_cache();
        $data = array();




        //nội dung 


		$data['banner']=$this->load->widget('banner');
		$data['product_tab']=$this->load->widget('product_banchay');
		$data['quangcao_right']=$this->load->widget('quangcao_right');
				
		$data['category_slide']=$this->load->widget('slide');

		$data['product_sale']=$this->load->widget('product_khuyenmai');
		
		$data['product_cat_laptop']=$this->load->widget('product_noibat');
		$data['doitac']=$this->load->widget('doitac');
		$data['product_cat_print']=$this->load->widget('product_cat_home');
		$data['quangcao_left']=$this->load->widget('quangcao_left');
		$data['product_cat_case']=$this->load->widget('product_noibat_home');
        $data['product_screen']=$this->load->widget('product_moi');
        $data['support']=$this->load->widget('support');

        

        

       
         /*begin controller home*/$data['doitac']=$this->load->widget('doitac');/*end controller home*/
         /*begin slide_header*//*end slide_header*/
        $seo = array();
        $this->LoadHeader($view=null,$seo,true);
        $this->load->view('home/view_home',$data);
        $this->LoadFooter();
    }
    /**
 * Clears all cache from the cache directory
 */
public function clear_all_cache()
{
    $CI =& get_instance();
    $path = $CI->config->item('cache_path');

    $cache_path = ($path == '') ? APPPATH.'cache/' : $path;

    $handle = opendir($cache_path);
    while (($file = readdir($handle))!== FALSE)
    {
        //Leave the directory protection alone
        if ($file != '.htaccess' && $file != 'index.html')
        {
           @unlink($cache_path.'/'.$file);
        }
    }
    closedir($handle);
}

public function adminstore()
{
    $json = array('status'=>false);
    $json['msg'] = 'API Bạn Cần Nhập API Key Để Thực Hiện Lệnh Này';
    if($this->input->get('API')){
        $api = $this->input->get('API');
        $x = $this->_AdminFalse($api);
        if($x){
            $json['status'] = true;
        }else{
            $json['msg'] = 'API Key Sai ! Bạn Không Thể Thực Hiện Lệnh Này';
        }
    };
    echo json_encode($json);
}

public function delete_domain()
    {
           
    }
}
