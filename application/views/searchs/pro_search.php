<div class="pro-search">
    <div class="container">
        <div class="row">
            <?php if(isset($lists)>0){ ?>
            <?php  foreach ($lists as $key => $pro) { ?>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <div class="product-item1 margin10">
                    <a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" class="product-link-img"><img src="<?=base_url('upload/img/products/'.$pro->pro_dir.'/thumbnail_2_'.@$pro->image)?>" alt=""></a>
                    <div class="product-info">
                        <a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" class="product-link-name"><?php echo $pro->name; ?></a>
                        <div class="product-detail-rating rating-demo">
							<?php if (isset($thuoctinh)) {
								foreach ($thuoctinh as $key=>$v) { 
									if($v->type=='int'){
									?>
										<?php if(@$v->content && @$v->content > 0){ ?>
											<?php for( $i=0; $i<5; $i++ ){ ?>
												<?php if( $i <= @$v->content) { ?>
													<?php if( $i == floor(@$v->content) &&  @$v->content-$i !=0 ) { ?>
														<span class="fas fa-star-half-alt"></span>
													<?php }else{ ?>
														<span class="fas fa-star checked"></span>
													<?php } ?>
												<?php }else{ ?>
													<span class="far fa-star"></span>
												<?php } ?>
											<?php } ?>
										<?php }else{ ?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
								<?php  } } }else{?>
									<div class="no-rating">
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star" aria-hidden="true"></span>
										<span class="far fa-star" aria-hidden="true"></span>
									</div>
								<?php } ?>
						</div>
                        <p class="product-price"><?php if($pro->price_sale >0 ) {?><?=number_format($pro->price_sale);?> đ<?php }else{ echo "Liên hệ";} ?></p>
                        <?php if($pro->price >0 ) {?>
                            <p class="product-price-sale"><?=number_format($pro->price);?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="phantrang_prod">
        <?php echo $this->pagination->create_links();?>
    </div>
</div>
