<style>
.product-similar-item {
    margin-bottom: 60px;
}

.product-similar-item .product-similar-image {
    display: inline-block;
    width: 100%;
    border: solid 1px #ebebeb;
    padding: 5px;
}

.product-similar-item .product-similar-image img {
    display: inline-block;
    width: 100%;
}

.product-similar-item .product-similar-info {
    text-align: center;
    padding: 15px 0;
}

.product-category-content .row-filter {
    display: flex;
	margin-bottom: 20px;
	border-bottom: none;
}

.product-category-content .row-filter .horizontal-list,
.product-category-content .row-filter .vetical-list {
    width: 50px;
    height: 50px;
    text-align: center;
    font-size: 20px;
    color: #777777;
    margin: 0 5px;
    cursor: pointer;
}
.row-filter .vetical-list a,.row-filter .horizontal-list a{
	width: 100%;
    height: 100%;
    display: inline-block;
	padding: 10px 0;
	border: solid 1px #afafaf;
}
.row-filter .vetical-list a:hover,.row-filter .horizontal-list a:hover{
	color: #0083c4;
    border: solid 1px #0083c4;
}
.row-filter .vetical-list a.active,.row-filter .horizontal-list a.active{
	color: #fff;
	border: solid 1px #0083c4;
	background:  #0083c4;
}
.menu-category-left,.menu-category-filter{
	padding: 0 20px;
	border: solid 1px #ebebeb;
	border-top: solid 2px #0083c4;

}
 .menu-category-title{
	font-size: 16px;
	padding: 13px 0 13px 0;
	font-family: 'Roboto-bold';
	margin-bottom: 0;
	position: relative;
}
.menu-category-title:before{
	content : '';
	background: #ebebeb;
	left: -20px;
	right: -20px;
	position: absolute;
	bottom: 0;
	height: 1px;
}
.menu-category-left .menu-category-list{
	padding: 8px 0 20px 0;
}
.menu-category-left .menu-category-list .menu-list-item {
	position: relative;
	padding: 8px 0;
}
.menu-category-left .menu-category-list .menu-list-item .btn-dropdown{
	position: absolute;
    top: 0;
    right: 0;
    width: 20px;
    height: 20px;
    cursor: pointer;
    padding: 5px 10px;
}
.menu-category-left .menu-category-list .menu-list-item  .menu-list-link{
	transition: .3s;
}
.menu-category-left .menu-category-list .menu-list-item.active .menu-list-link,.menu-category-left .menu-category-list .menu-list-item:hover  .menu-list-link{
	color: #0083c4;
}
.menu-category-child{
	margin: 5px 0 0 15px;
	height: 0;
	opacity: 0;
	
}
.menu-child-active .menu-category-child{
	height: auto;
	opacity: 1;
	transition: .5s;
}
.menu-category-child .category-child-item{
	padding: 5px 0;
}
.menu-category-child .category-child-item .menu-child-link{
	color: #848484;
}
.filter-price-scroll{
	width: 100%;
	position: relative;
	margin: 8px 0 20px 0;
}
.filter-price-scroll .filter-input{
	position: absolute;
	width: 100%;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	-webkit-appearance: none;
	background: #efefef;
	border-radius: 10px;
	height: 18px;
	border: solid 1px #989898;

}
.filter-price-scroll .filter-input:focus, .filter-price-scroll .filter-input:active{
	outline: none;
}
.filter-price-scroll .filter-input::-webkit-slider-thumb { 
  /* WebKit/Blink */
    position: relative;
    margin: 0;
    border: 0;
    outline: 0;
    border-radius: 50%;
    height: 10px;
    width: 10px;
    background-color: #fff;
    cursor: pointer;
    cursor: pointer;
    pointer-events: all;
    z-index: 100;
}

.filter-price-scroll .filter-input::-moz-range-thumb { 
  /* Firefox */
  position: relative;
  appearance: none;
  margin: 0;
  border: 0;
  outline: 0;
  border-radius: 50%;
  height: 10px;
  width: 10px;
  background-color: #fff;
  cursor: pointer;
  cursor: pointer;
  pointer-events: all;
  z-index: 100;
}

.filter-price-scroll .filter-input::-ms-thumb  { 
  /* IE */
  position: relative;
  appearance: none;
  margin: 0;
  border: 0;
  outline: 0;
  border-radius: 50%;
  height: 10px;
  width: 10px;
  background-color: #242424;
  cursor: pointer;
  cursor: pointer;
  pointer-events: all;
  z-index: 100;
}
.filter-price-scroll .price-wrap-1{
	position: absolute;
	top : 20px;
	left: 0;
}
.filter-price-scroll .price-wrap-2{
	position: absolute;
	top : 20px;
	right: 0;
	
}
.filter-price-scroll .price-wrap-2 input{
	text-align: right;
}
.filter-price-scroll .price-wrap-1 input,.filter-price-scroll .price-wrap-2 input{
	border: none;
	background: none;
	font-family: 'Roboto-medium';
	color: #989898;
}
.filter-price-scroll .price-wrap-1 input:focus,.filter-price-scroll .price-wrap-2 input:focus{
	outline:none;
	cursor:none;
	pointer-events: none;
}
.menu-category-filter button{
	border: solid 1px #989898;
	margin-top: 40px;
	text-transform: uppercase;
}
.menu-category-filter button:focus{
	outline:none;
}
.menu-category-filter{
	padding : 0 20px;
}
div.pagination{
	margin: 40px 0;

}
.phantrang{
	margin: 0 auto;
}
.phantrang li  a{
	display: inline-block;
    padding: 5px 15px;
    border: solid 1px #afafaf;
    margin: 0 5px;
}
.phantrang li.active  a{
	background: #0083c4;
	color: #fff;
}
.list-product-vetical .product-vetical-item{
	height: 250px;
	margin-bottom: 45px;
}
.list-product-vetical .product-vetical-item .product-vetical-link{
	display: inline-block;
    width: 30%;
    overflow: hidden;
    padding: 0 10px;
    float: left;
    height: 100%;
    border: solid 1px #efefef;
}

.list-product-vetical .product-vetical-item .product-vetical-link img{
	width: 100%;
    height: 100%;
}
.list-product-vetical .product-vetical-item .product-vetical-info{
	padding: 5px 10px;
    width: 65%;
    float: left;
}
.list-product-vetical .product-vetical-item .product-vetical-info .product-vetical-name{
	font-size: 26px;
	font-family: 'Roboto-Medium';
	color: #202020;
}
.list-product-vetical .product-vetical-item .product-vetical-info .product-description-price{
	font-size: 24px;
	margin: 20px 0;
}
.product-category-content .price-demo .price-old{
	font-size: 16px;

}
.list-product-vetical .product-vetical-item .product-vetical-info .product-vetical-description{
	color: #848484;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
}
</style>
<div class="product-category">
    <div class="container-ics">
        <div class="back-link">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item" > <i class="fa fa-home" aria-hidden="true"></i><a href="#">Trang chủ</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page"><?= $cate_curent->name?></li>
                </ol>
            </nav>
        </div>
        <div class="product-category-content">

            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="menu-category-left">
						<h3 class="menu-category-title">CATEGORIES</h3>
						<div class="menu-category-list">
							<?php if (isset($all_cate)) { ?>
								<ul class="category-list">
									<?php foreach ($all_cate as $cate_parent) { ?>
										<li class="menu-list-item ">
											<a href="<?=base_url('danh-muc/'.$cate_parent->alias.'.html')?>" title="<?=$cate_parent->name?>" class="menu-list-link"><?=$cate_parent->name?> (<?=$cate_parent->count_pro?>)</a>
											<?php if (isset($cate_parent->cate_child) && count($cate_parent->cate_child)>0) { ?>
												<div class="btn-dropdown">
													<i class="fas fa-sort-down btn-dropdown-icon"></i>
												</div>
												<ul class="menu-category-child">
													<?php foreach ($cate_parent->cate_child as $key => $n) { ?>
														<li class="category-child-item"><a href="<?=base_url('danh-muc/'.$n->alias.'.html')?>" title="<?=$n->name?>" class="menu-child-link"><?=$n->name?> (<?=$n->count_pro?>)</a></li>
													<?php } ?>
													
												</ul>
											<?php } ?>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>
						</div>
                    </div>
					<div class="menu-category-filter">
						<h3 class="menu-category-title">FILTER BY PRICE</h3>
						<div class="filter-price-scroll">
								<input type="range" class="filter-input filter-input-min" min="0" max="50000000" value="500000" id="lower">
								<input type="range" class="filter-input filter-input-max" min="0" max="50000000" value="5000000" id="upper">
								<div class="price-wrap-1">
									<input id="min-value">
								</div>
								<div class="price-wrap_line">-</div>
								<div class="price-wrap-2">
									<input id="max-value">
								</div>
								<button>filter</button>
						</div>
						
					</div>
                </div>

                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
                    <ul class="row-filter  nav nav-tabs">
						
                        <li class="vetical-list">
                            <a href="#horizontal" class="active" data-toggle="tab"><i class="fas fa-th-list"></i></a>
                        </li>
                        <li class="horizontal-list">
							<a href="#vetical"  data-toggle="tab"><i class="fas fa-th"></i></a>
                        </li>
					</ul>
                    <div class="row-list-product tab-content">
						<div class="tab-pane fade in active show"  id="horizontal">								
							<div class="row">
								<?php if(isset($lists)){ ?>
								<?php foreach($lists as $pro) : ?>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<div class="product-similar-item">
										<a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>"
											class="product-similar-image"><img
												src="<?=base_url('upload/img/products/'.$pro->pro_dir.'/thumbnail_2_'.@$pro->image)?>"
												alt=""></a>
										<div class="product-similar-info">
											<a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>"
												class="product-similar-link name-product-current"><?php echo $pro->name; ?></a>
											<div class="product-similar-rating rating-demo">
												<?php if (isset($pro->rating)) { ?>
														<?php if(@$pro->rating > 0){ ?>
															<?php for( $i=0; $i<5; $i++ ){ ?>
																<?php if( $i <= @$pro->rating) { ?>
																	<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																		<span class="fas fa-star-half-alt"></span>
																	<?php }else{ ?>
																		<span class="fas fa-star checked"></span>
																	<?php } ?>
																<?php }else{ ?>
																	<span class="far fa-star"></span>
																<?php } ?>
															<?php } ?>
														<?php }else{ ?>
															<div class="no-rating">
																<span class="far fa-star"></span>
																<span class="far fa-star"></span>
																<span class="far fa-star"></span>
																<span class="far fa-star" aria-hidden="true"></span>
																<span class="far fa-star" aria-hidden="true"></span>
															</div>
														<?php } ?>
												<?php  }else{?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
											</div>
											<div class="product-similar-price price-demo">
											<?php if($pro->price && $pro->price>0){ ?><span class="price-old"><?=number_format($pro->price)?></span><?php } ?><?=number_format($pro->price_sale)?>VND
											</div>
										</div>
									</div>
								</div>
								<?php endforeach;?>
								<?php }?>
							</div>
						</div>
                        
						<div class="list-product-vetical tab-pane fade" id="vetical">
							<?php if(isset($lists)){ ?>
							<?php foreach($lists as $pro) : ?>
								<div class="product-vetical-item">
									<a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" class="product-vetical-link"><img src="<?=base_url('upload/img/products/'.$pro->pro_dir.'/thumbnail_2_'.@$pro->image)?>" alt=""></a>
									<div class="product-vetical-info">
										<a href="<?= base_url('san-pham/'.$pro->alias.'.html') ?>" class="product-vetical-name"><?php echo $pro->name; ?></a>
										<div class="product-vetical-rating rating-demo">
											<?php if (isset($pro->rating)) { ?>
													<?php if(@$pro->rating > 0){ ?>
														<?php for( $i=0; $i<5; $i++ ){ ?>
															<?php if( $i <= @$pro->rating) { ?>
																<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																	<span class="fas fa-star-half-alt"></span>
																<?php }else{ ?>
																	<span class="fas fa-star checked"></span>
																<?php } ?>
															<?php }else{ ?>
																<span class="far fa-star"></span>
															<?php } ?>
														<?php } ?>
													<?php }else{ ?>
														<div class="no-rating">
															<span class="far fa-star"></span>
															<span class="far fa-star"></span>
															<span class="far fa-star"></span>
															<span class="far fa-star" aria-hidden="true"></span>
															<span class="far fa-star" aria-hidden="true"></span>
														</div>
													<?php } ?>
											<?php  }else{?>
												<div class="no-rating">
													<span class="far fa-star"></span>
													<span class="far fa-star"></span>
													<span class="far fa-star"></span>
													<span class="far fa-star" aria-hidden="true"></span>
													<span class="far fa-star" aria-hidden="true"></span>
												</div>
											<?php } ?>
										</div>
										<div class="product-description-price price-demo">
											<?php if($pro->price && $pro->price>0){ ?><span class="price-old"><?=number_format($pro->price)?></span><?php } ?> <?=number_format($pro->price_sale)?>VND
										</div>
										<div class="product-vetical-description">
										<?php echo $pro->description; ?>
										</div>
									</div>
								</div>
							<?php endforeach;?>
                            <?php }?>
						</div>
                    </div>
                </div>

            </div>

        </div>
		<div class="pagination">
			<?php
				echo $this->pagination->create_links(); // tạo link phân trang
				?>
		</div>
    </div>
</div>

<script>
	$(document).ready(function() {
		$('.btn-dropdown').click(function (e) { 
			$(this).parent().toggleClass('menu-child-active');
			$(this).find('.btn-dropdown-icon').toggleClass('fas fa-sort-down').toggleClass('fas fa-sort-up');
		});
		var lowerSlider = document.querySelector('#lower');
		var  upperSlider = document.querySelector('#upper');

		document.querySelector('#max-value').value=new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(upperSlider.value);
		document.querySelector('#min-value').value=new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(lowerSlider.value);

		var  lowerVal = parseInt(lowerSlider.value);
		var upperVal = parseInt(upperSlider.value);

		upperSlider.oninput = function () {
			lowerVal = parseInt(lowerSlider.value);
			upperVal = parseInt(upperSlider.value);

			if (upperVal < lowerVal + 4) {
				lowerSlider.value = upperVal - 4;
				if (lowerVal == lowerSlider.min) {
				upperSlider.value = 4;
				}
			}
			document.querySelector('#max-value').value=new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(this.value);
		};

		lowerSlider.oninput = function () {
			lowerVal = parseInt(lowerSlider.value);
			upperVal = parseInt(upperSlider.value);
			if (lowerVal > upperVal - 4) {
				upperSlider.value = lowerVal + 4;
				if (upperVal == upperSlider.max) {
					lowerSlider.value = parseInt(upperSlider.max) - 4;
				}
			}
			document.querySelector('#min-value').value=new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(this.value);
		}; 
	});
</script>
