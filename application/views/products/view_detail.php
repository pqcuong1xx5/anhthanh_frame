
<div class="product-detail">
	<div class="container">
		<div class="back-link">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"> <i class="fa fa-home" aria-hidden="true"></i><a href="#"></a></li>
					<?php if(isset($cate_current)){ ?>
					<li class="breadcrumb-item"><a href="<?=base_url('danh-muc/'.$cate_current->alias.'.html') ?>"><?= $cate_current->name?></a></li>
					<?php } ?>
					<li class="breadcrumb-item active" aria-current="page"><?= $item->name?></li>
				</ol>
			</nav>
		</div>
		<div class="product-detail-content">
			
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div class="product-image-slider">
						<div class="slider-for ">
							<?php if(!empty($item->image)){ ?>
								<div class="slider-for__item ex1" data-src="https://unsplash.it/870/870">
									<div class="slider-image-zoom">
									<img src="<?= base_url('upload/img/products/' . $item->pro_dir . '/' . @$item->image) ?>" alt="">
									</div>
								</div>
							<?php } ?>
							<?php if (isset($p_images)): ?>
								<?php foreach ($p_images as $key => $v): ?>
								<div class="slider-for__item ex1" data-src="https://unsplash.it/871/870">
									<div class="slider-image-zoom">
										<img src="<?= base_url(@$v->image) ?>" alt="">
									</div>
								</div>
								<?php endforeach ?>
							<?php endif ?>
							</div>
						</div>
						<div class="slider-nav">
							<?php if(!empty($item->image)){ ?>
								<div class="slider-nav__item">
									<div class="product-detail-img">
										<img src="<?= base_url('upload/img/products/' . $item->pro_dir . '/' . @$item->image) ?>" alt="">
									</div>
								</div>
							<?php } ?>
							<?php if (isset($p_images)): ?>
								<?php foreach ($p_images as $key => $v): ?>
								<div class="slider-nav__item">
									<div class="product-detail-img">
									<img src="<?= base_url(@$v->image) ?>" alt="">
									</div>
								</div>
								<?php endforeach ?>
							<?php endif ?>
						</div>
					</div>
				
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
					<div class="product-info">
						<h3 class="product-name"><?=$item->name; ?></h3>
						<div class="product-detail-rating rating-demo">
							<?php if (isset($thuoctinh)) {
								foreach ($thuoctinh as $key=>$v) { 
									if($v->type=='int'){
									?>
										<?php if(@$v->content && @$v->content > 0){ ?>
											<?php for( $i=0; $i<5; $i++ ){ ?>
												<?php if( $i <= @$v->content) { ?>
													<?php if( $i == floor(@$v->content) &&  @$v->content-$i !=0 ) { ?>
														<span class="fas fa-star-half-alt"></span>
													<?php }else{ ?>
														<span class="fas fa-star checked"></span>
													<?php } ?>
												<?php }else{ ?>
													<span class="far fa-star"></span>
												<?php } ?>
											<?php } ?>
										<?php }else{ ?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
								<?php  } } }else{?>
									<div class="no-rating">
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star" aria-hidden="true"></span>
										<span class="far fa-star" aria-hidden="true"></span>
									</div>
								<?php } ?>
							</div>
						<div class="product-price"><?=number_format($item->price_sale)?> đ</div>
						<div class="price-note">(Giá đã bao gồm VAT)</div>
						<div class="button-cart">
							<button class="btn buy-now">Mua ngay</button>
							<button class="btn add-to-cart">Thêm vào giỏ</button>
						</div>
						<div class="product-promotion">
							<div class="promotion-title">Khuyễn mãi</div>
							<div class="promotion-content">
								<p>Tặng túi đựng laptop thời trang.</p>
								<p>Nhận thêm gói vệ sinh laptop miễn phí tại Tuấn Thành.</p>
								<p>Freeship với đơn hàng nội thành Hà Nội.</p>
								<p><strong>Duy nhất trong tháng 12/2020</strong></p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					<div class="description-pro">
						<?=$item->description; ?>
					</div>
				</div>
				
			</div>
		</div>
		<div class="product-similar">
			<div class="product-title">
				<h3 class="similar-title">SẢN PHẨM TƯƠNG TỰ</h3>
			</div>
			<div class="similar-slider">
				<?php if(isset($list_item)):
					foreach($list_item as $pro):  ?>
				<div class="product-item1 margin10">
					<a href="<?=base_url('san-pham/'.$pro->alias);?>" class="product-link-img"><img src="<?= base_url('upload/img/products/'.$pro->pro_dir.'/'.$pro->image) ?>" alt=""></a>
					<div class="product-info">
						<a href="<?=base_url('san-pham/'.$pro->alias);?>" class="product-link-name"><?=$pro->name;?></a>
						<div class="product-detail-rating rating-demo">
							<?php if (isset($thuoctinh)) {
								foreach ($thuoctinh as $key=>$v) { 
									if($v->type=='int'){
									?>
										<?php if(@$v->content && @$v->content > 0){ ?>
											<?php for( $i=0; $i<5; $i++ ){ ?>
												<?php if( $i <= @$v->content) { ?>
													<?php if( $i == floor(@$v->content) &&  @$v->content-$i !=0 ) { ?>
														<span class="fas fa-star-half-alt"></span>
													<?php }else{ ?>
														<span class="fas fa-star checked"></span>
													<?php } ?>
												<?php }else{ ?>
													<span class="far fa-star"></span>
												<?php } ?>
											<?php } ?>
										<?php }else{ ?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
								<?php  } } }else{?>
									<div class="no-rating">
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star" aria-hidden="true"></span>
										<span class="far fa-star" aria-hidden="true"></span>
									</div>
								<?php } ?>
						</div>
						<p class="product-price"><?php if($pro->price_sale >0 ) {?><?=number_format($pro->price_sale);?> đ<?php }else{ echo "Liên hệ";} ?></p>
						<?php if($pro->price >0 ) {?>
							<p class="product-price-sale"><?=number_format($pro->price);?></p>
						<?php } ?>
					</div>
				</div>
				<?php endforeach;endif;?> 
			</div>
		</div>
		<div class="product-content" id="product-content">
			
			<div class="row">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div class="content-left">
						<h3 class="content-title">MÔ TẢ SẢN PHẨM</h3>
						<div class="content-detail">
						<?php if (isset($thuoctinh)) {
						foreach ($thuoctinh as $key=>$v) { 
							if($v->type=='textarea'){
							?>
							<?=@$v->content;?>
						<?php }  } }?>
						</div>
					</div>
				</div>
				
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div class="content-right">
						<h3 class="content-title">THÔNG SỐ KỸ THUẬT</h3>
						<div class="content-right-detail">
							<div class="content-right-item">
								<div class="item-left">Thương hiệu</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Bảo hành</div>
								<div class="item-right">12 tháng</div>
							</div>
							<h4 class="item-title">Thông tin chung</h4>
							<div class="content-right-item">
								<div class="item-left">Tên sản phẩm</div>
								<div class="item-right">ThinkBook 13s-IWL 20R900DHVN</div>
							</div>
							<h4 class="item-title">Cấu hình chi tiết</h4>
							<div class="content-right-item">
								<div class="item-left">Part-number</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Bộ vi xử lý</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Bộ nhớ chính (RAM)</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Dung lương lưu trữ</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Card đồ họa	</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Cổng giao tiếp</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Kết nối không dây	</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Camera</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Màn hình</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Nguồn</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Kích thước / trọng lượng</div>
								<div class="item-right">LENOVO</div>
							</div>
							<div class="content-right-item">
								<div class="item-left">Mầu sắc/ xuất sứ</div>
								<div class="item-right">LENOVO</div>
							</div>
							<h4 class="item-title">Thông tin khác</h4>
						</div>
					</div>
				</div>
				
			</div>
			<div class="btn-show">
				<button class="btn btn-content btn-show-content">Xem chi tiết</button>
				<button class="btn btn-content btn-hidden-content">Ẩn	 chi tiết</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		$('.product-add-cart').click(function (e) { 
			var quantity = document.getElementById('quantity-pro').value;
			var id = document.getElementById('id-pro').value;

			$.ajax({
		      		type: "POST",
		      		url: "<?php echo base_url('add-to-cart'); ?>",
		      		data: { product: id,qty: quantity },
		      		success: function(result){
						$('.messenter_popup').addClass('messager_active');
		      			setTimeout(function(){
		    				$('.messenter_popup').removeClass('messager_active');
						}, 2000);
						$('#quant-cart').html(result);
		      		}
			})
		});
		$('.btn-quantity-up').click(function (e) { 
			
			var quantity = document.getElementById('quantity-pro').value;
			quantity++;
			document.getElementById('quantity-pro').value = quantity;
			
		});
		$('.btn-quantity-down').click(function (e) { 
			
			var quantity = document.getElementById('quantity-pro').value;
			if(quantity<=1){
				return;
			}
			quantity--;
			document.getElementById('quantity-pro').value = quantity;
			
		});
		
		
	});
	
</script>
