<div class="row marginrow10 top-product-slider">
	<?php foreach($pros as $k => $pro){ ?>
	<div class="col-sm-2dot4" style="max-width: auto;">
		<div class="product-item1">
			<a href="<?= base_url('san-pham/'.$pro->alias)?>" class="product-link-img"><img src="<?= base_url('upload/img/products/'.$pro->pro_dir.'/'.$pro->image) ?>" alt=""></a>
			<div class="product-info">
				<a href="<?= base_url('san-pham/'.$pro->alias)?>" class="product-link-name"><?=$pro->name;?></a>
				<div class="product-cart">
					<div class="product-electronic-rating rating-demo">
						<?php if (isset($pro->rating)) { ?>
								<?php if(@$pro->rating > 0){ ?>
									<?php for( $i=0; $i<5; $i++ ){ ?>
										<?php if( $i <= @$pro->rating) { ?>
											<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
												<span class="fas fa-star-half-alt"></span>
											<?php }else{ ?>
												<span class="fas fa-star checked"></span>
											<?php } ?>
										<?php }else{ ?>
											<span class="far fa-star"></span>
										<?php } ?>
									<?php } ?>
								<?php }else{ ?>
									<div class="no-rating">
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star"></span>
										<span class="far fa-star" aria-hidden="true"></span>
										<span class="far fa-star" aria-hidden="true"></span>
									</div>
								<?php } ?>
						<?php  }else{?>
							<div class="no-rating">
								<span class="far fa-star"></span>
								<span class="far fa-star"></span>
								<span class="far fa-star"></span>
								<span class="far fa-star" aria-hidden="true"></span>
								<span class="far fa-star" aria-hidden="true"></span>
							</div>
						<?php } ?>
					</div>
					<a href="" class="btn"><i class="fas fa-cart-plus"></i> </a>
				</div>
				<p class="product-price"><?php if($pro->price_sale >0 ) {?><?=number_format($pro->price_sale);?> đ<?php }else{ echo "Liên hệ";} ?></p>
				<?php if($pro->price >0 ) {?>
					<p class="product-price-sale"><?=number_format($pro->price);?></p>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<script>
	$('.top-product-slider').slick({
        infinit: true,
        dots: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-sliders btn-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-sliders btn-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
        responsive: [{
                breakpoint: 1600,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>
