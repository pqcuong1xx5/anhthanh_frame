<style type="text/css">
   .back_link ul li{
      display: inline-block;
   }
   .back_link ul li a{
      display: block;
      color: #7e7e7e;
      font-size: 14px;
      padding: 0 7px;
      position: relative;
   }
   .back_link ul li:not(:last-child) a:after{
      position: absolute;
      content: '>';
      display: inline-block;
      top: 0;
      right: -6px;
   }
   .back_link ul li:first-child a{
      padding-left: 0;
	}
	.row-item-new{
		padding: 20px 0;
		border-bottom: solid 1px #ebebeb;
		position: relative;
	}
   .title_detail {
      font-size: 18px;
      color: #000;
      font-weight: bold;
		margin-bottom: 7px;
		border-bottom: solid 1px #ebebeb;
		padding: 20px 0;
		margin-bottom: 20px;
	}
   .back_link{
      padding-bottom: 7px;
      border-bottom: 1px solid #ddd;
   }
   .content{
      font-size: 14px;
      color: 000;
      line-height: 22px;
      text-align: justify;
   }
   .content img{
      max-width: 100%;
      display: block;
      margin: 10px auto;
   }
   .tit-content{
      font-weight: bold;
      display: block;
   }
   .news-lq h3{
      position: relative;
	}
	.height100{
		height: 100%;
	}
   .news-lq h3 img{
      float: none !important;
      width: 24px !important;
      height: auto !important;
	}
	.item-new-img{
		display: inline-block;
    width: 100%;
    height: 100%;
	}
	.item-new-img img{
		width: 100%;
    height: 100%;
	}
	.row-item-new{
		height: 300px;
	}
	.view-pros-page{
		margin: 20px 0;
	}
	.item-new-name{
		font-family: 'Roboto-Bold';
	 font-size: 24px;
	 overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    display: -webkit-box;
	}
	.tomtat-baiviet{
		overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    display: -webkit-box;
	}
	.btn-view-page{
		position: absolute;
		bottom: 0;
		right: 0;
	}
   .list-video {
      padding: 0px 10px 26px 0px;
      background: #fff; }
      .list-video li {
         margin-bottom: 20px;
         padding-left: 0 !important; }
         .list-video li:last-child {
            margin-bottom: 0; }
            .list-video li:hover h3 {
               color: red; }
               .list-video li a {
                  display: block; }
                  .list-video li a img {
                     width: 89px;
                     height: 60px;
                     float: left;
                     margin-right: 7px; }
                     .list-video li a h3 {
                        font-size: 14px;
                        color: #000;
                        line-height: 24px;
                        max-height: 60px;
                        overflow: hidden;
                        -webkit-box-orient: vertical;
                        text-overflow: ellipsis;
                        white-space: normal;
                        -webkit-line-clamp: 2;
                        display: -webkit-box; }
                        .content_news {
                           padding: 10px 0;
                        }
                     </style>
<div class="clearfix"></div>
<!-- begin left_content --> <!-- end left_content --><!-- begin mid_content -->
<div class="qts-mid-content">
   <div class="container-ics">

<div class="back-link">
         <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
               <li class="breadcrumb-item"> <i class="fa fa-home" aria-hidden="true"></i><a href="<?=base_url()?>" title="">Trang chủ</a></li>
               <li class="breadcrumb-item"><a href="<?=base_url('danh-muc-tin')?>">Tin tức</a></li>
               <li class="breadcrumb-item active" aria-current="page"><?=$cate_current->name?></li>
            </ol>
         </nav>
      </div>

      <div class="clearfix-10"></div>
      <h2 class="title_detail"><?= $news->title?></h2>
      <div class="post-date"><?=@$weekday?></div>
      <div class="clearfix-15"></div>
      <div class="content content_news">
         <span class="tit-content"><?=$news->description?></span>
         <div class="clearfix-10"></div>
         <?= $news->content?>
      </div>
      <div class="clearfix-50"></div>
      <div class="news-lq">
         <h2 class="title_detail"><a href="javascript:void(0)">Bài viết liên quan</a></h2>
         <div class="clearfix-15"></div>
         <div class="list-video clearfix">
            <?php if (count($new_same)) : ?>
               <?php foreach ($new_same as $new) : ?>
					
					<div class="row row-item-new">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 height100">
							<a href="<?=base_url('new/'.$new->alias.'.html')?>" class="item-new-img">
                        <img src="<?php echo base_url($new->image) ?>">
							</a>
						</div>
						
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
							<h3 class="item-new-name"><?=$new->title?> </h3>
							<div class="view-pros-page clearfix">
								<span class="date-time pull-left"><?= date('d',$new->time)?> Tháng <?= date('m',$new->time)?>, <?= date('Y',$new->time)?></span>
								
                     </div>
							<div class="tomtat-baiviet">
								<?=$new->description?>
							</div>
							<a href="<?= base_url('new/'.$new->alias.'.html')?>" title="" class="btn btn-view-page pull-right">Xem thêm <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						</div>
						
					</div>

               <?php endforeach; ?>
            <?php endif; ?>
         </div>
      </div>
   </div>
</div>


