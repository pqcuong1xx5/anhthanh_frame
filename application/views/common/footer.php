
<footer>
	<div class="footerfb">
		<div class="footer-fb">
			<a href="" class="footer-fb-link"><strong>cuongpq.com.vn</strong></a>
		</div>
	</div>
	<div class="container">
		<div class="footer-content">
			<div class="footer-logo">
				<a href=""><img src="<?=@$this->option->site_logo_footer;?>" alt=""></a>
			</div>
			<div class="footer-category">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 lineh17">
						<p class="computer-name"><?=@$this->option->slogan;?></p>
						<p class="footer-address">Địa chỉ: <?=@$this->option->address;?> </p>
						<p class="computer-name">Tel: <?=@$this->option->hotline1;?>  - <?=@$this->option->hotline2;?> </p>
						<p class="computer-name">E-mail: <?=@$this->option->email;?></p>
						<p class="computer-name">GPKD số: 0101107422 do Sở Kế hoạch và Đầu tư Thành phố Hà nội cấp</p>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
						<ul class="store-list">
							<li class="store-item"><a href="" title="">FAQ</a></li>
							<li class="store-item"><a href="" title="">Gian hàng shoppe</a></li>
							<li class="store-item"><a href="" title="">Gian hàng lazada</a></li>
							<li class="store-item"><a href="" title="">Gian hàng TIKI</a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="footer-chungnhan">
							<img src="img/tbbct.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</footer>
<div class="coppy-right">
	<p>Copyright © 2020 Cường PQ Informatics Co., Ltd.</p>
</div>
<div id="scrollToTop" class="scrollToTop "></div>
<div class="ketqua">
	<h2 class='messenter_popup'>Thêm vào giỏ thành công</h2>
</div>
<!-- <div class="ketqua">
	<h2 class='messenter_popup'>Cập nhập giỏ thành công</h2>
</div> -->
<div class="sp-cart">
	<a href="<?=base_url('gio-hang') ?>">
		<i class="fa fa-shopping-cart ic-cart" aria-hidden="true"></i>
		<i class="quant-cart">
			<span><?=$cart ? count($cart) : '0';?></span>
		</i>
	</a>
</div>
<div class="chat-faceboook">
			<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="635977516920487"
  logged_in_greeting="Xin chào! Bạn cần hỗ trợ gì ?"
  logged_out_greeting="Xin chào! Bạn cần hỗ trợ gì ?">
      </div>
</div>	
	<script src="<?=base_url()?>assets/js/front_end/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/slick.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/fontawesome.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/all.min.js"></script>
    <script src="<?=base_url()?>assets/js/front_end/main.js"></script>
	  <script>
		function addcart(value) {
			$.ajax({
				type: "POST",
				url: "<?=base_url('add-to-cart')?>",
				data: { product: value },
				success: function(result) {
					$('.messenter_popup').addClass('messager_active');
					setTimeout(function() {
						$('.messenter_popup').removeClass('messager_active');
					}, 2000);
					$('.quant-cart').html(result);
					$('#header-cart').html(result);
				}
			})
		}
		function getProductNew(value) {
			$('#top-pro-loading').addClass('loading')
			$('#product_top').addClass('hidden_view')
			$.ajax({
				type: "GET",
				url: "<?=base_url('new-product')?>",
				data: { type: value },
				success: function(result) {
					setTimeout(function() {
						$('#top-pro-loading').removeClass('loading')
						$('#product_top').removeClass('hidden_view')
						$('#product_top').html(result);
					}, 2000);
				}
			})
		}
		function getProductByCategory(value) {
			$('#laptop-pro-loading').addClass('loading')
			$('#laptop-product').addClass('hidden_view')
			$.ajax({
				type: "GET",
				url: "<?=base_url('product-category')?>",
				data: { cat_id: value },
				success: function(result) {
					setTimeout(function() {
						$('#laptop-pro-loading').removeClass('loading')
						$('#laptop-product').removeClass('hidden_view')
						$('#laptop-product').html(result);
					}, 2000);
				}
			})
		}
	  </script>
<!-- end footer -->
<!-- begin container_close_footer --><!-- end container_close_footer -->
<?php if(isset($this->config_hotline)){?>
<?=$this->load->view('temp/hotline');?>
<?php } ?>


<?php if(isset($this->config_chatfanpage)){?>
<?=$this->load->view('temp/chat_fanpage');?><?php } ?>
<div id="show_success_mss" style="position: fixed; top: 40px; right: 20px;z-index:9999;">
    <?php if($this->session->flashdata('mess')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-success"></i><?=$this->session->flashdata('mess'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('mess_err')): ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-warning"></i><?=$this->session->flashdata('mess_err'); ?>
    </div>
    <?php endif; ?>
</div>
<link href="<?= base_url()?>assets/plugin/ValidationEngine/css/validationEngine.jquery.css" rel="stylesheet"/>
<script src="<?= base_url()?>assets/plugin/ValidationEngine/js/jquery.validationEngine-vi.js"
            charset="utf-8"></script>
<script src="<?= base_url()?>assets/plugin/ValidationEngine/js/jquery.validationEngine.js"></script>
<script src="<?= base_url()?>assets/js/front_end/script.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
		$('#loading-page').removeClass('se-pre-con');
        $('.validate ').validationEngine();
    });
     setTimeout(function(){
        $('#show_success_mss').fadeOut().empty();
    }, 9000);
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/front_end/owl.carousel2.min.js"></script>
</body>
</html>
