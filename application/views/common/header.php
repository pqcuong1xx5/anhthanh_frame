<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <title><?= isset($seo['title']) && $seo['title'] != '' ? $seo['title'] : @$this->option->site_name; ?></title>
    <link rel="shortcut icon" href="<?= base_url(@$this->option->favicon ) ?>"/>
    <meta name='description'
          content='<?= isset($seo['description']) ? $seo['description'] : @$this->option->site_description; ?>'/>
    <meta name='keywords'
          content='<?= isset($seo['keyword']) && $seo['keyword'] != '' ? $seo['keyword'] : $this->option->site_keyword; ?>'/>
    <meta name='robots' content='index,follow'/>
    <meta name='revisit-after' content='1 days'/>
    <meta http-equiv='content-language' content='vi'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="126821687974504" />
    <meta property="fb:admins" content="100006472503973"/>

    <link rel="canonical" href="<?=current_url();?>" />
    <!--    for facebook-->
    <meta property="og:title"
          content="<?= isset($seo['title']) && $seo['title'] != '' ? $seo['title'] : @$this->option->site_name; ?>"/>
    <meta property="og:site_name" content="<?= @$this->option->site_name; ?>"/>
    <meta property="og:url" content="<?= current_url(); ?>"/>
    <meta property="og:description"
          content="<?= isset($seo['description']) && $seo['description'] != '' ? $seo['description'] : @$this->option->site_description; ?>"/>
    <meta property="og:type" content="<?= @$seo['type']; ?>"/>
    <meta property="og:image" content="<?= isset($seo['image']) && @$seo['image'] != '' ? base_url(@$seo['image']) : @$this->option->site_logo ; ?>"/>

    <meta property="og:locale" content="vi_VN"/>

    <!-- for Twitter -->
    <meta name="twitter:card"
          content="<?= isset($seo['description']) && $seo['description'] != '' ? $seo['description'] : @$this->option->site_description; ?>"/>
    <meta name="twitter:title"
          content="<?= isset($seo['title']) && $seo['title'] != '' ? $seo['title'] : @$this->option->site_name; ?>"/>
    <meta name="twitter:description"
          content="<?= isset($seo['description']) && $seo['description'] != '' ? $seo['description'] : @$this->option->site_description; ?>"/>
    <meta name="twitter:image"
          content="<?= isset($seo['image']) && $seo['image'] != '' ? base_url($seo['image']) : base_url(@$this->option->site_logo); ?>"/>
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/bootstrap.min.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/bootstrap-grid.min.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/slick-theme.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/slick.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/fontawesome.min.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/all.min.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/animate.min.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/main.css">
      <link rel="stylesheet" media="screen" href="<?=base_url()?>assets/css/front_end/media.css">
      <script src="<?=base_url()?>assets/js/front_end/jquery-3.5.1.min.js"></script>
      <script src="<?=base_url()?>assets/js/front_end/jquery.zoom.min.js"></script>

  <input type="hidden" value="<?= base_url()?>" id="base_url" name="">

</head>

<body id="homepage">
<style>

.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url("<?=base_url();?>img/Preloader_3.gif") center no-repeat #fff;
}
</style>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<!-- <script>
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");;
	});
</script> -->
<div class="se-pre-con" id="loading-page"></div>



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=126821687974504";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- begin container_header --><!-- end container_header -->
<section class="loading loading-body" id="section-loading">
    <div class="loader loader-2">
      <svg class="loader-star" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
            <polygon points="29.8 0.3 22.8 21.8 0 21.8 18.5 35.2 11.5 56.7 29.8 43.4 48.2 56.7 41.2 35.1 59.6 21.8 36.8 21.8 " fill="#18ffff" />
         </svg>
      <div class="loader-circles"></div>
    </div>
  </section>
<?=$menu_main;?>
<header id="header">
      <div class="container">
            <div class="header-content">
                  <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4 col-no-scoll">
                              <div class="header-logo">
                                    <a href="<?= base_url()?>" class="logo-link"><img src="<?= base_url(@$this->option->site_logo)?>" alt=""></a>
                              </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-scoll">
					<div class="header-logo2">
                                    <a href="" class="logo-link2"><img src="img/logo2.png" alt=""></a>
                              </div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3 col-scoll">
                              <div class="header-btn-menu">
                                    <span class=" btn-open open-menu-scroll"><i>☰</i><b> DANH MỤC SẢN PHẨM</b></span>
                              </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="header-search">
						<span class="btn-menu"><i>☰</i></span>
						<form action="<?=base_url('tim-kiem')?>" method="GET">
							<input type="text" name="s" id="input" placeholder="Tìm kiếm" class="form-control header-input" value="" >
							<button type="submit" class="header-btn-search"></button>
						</form>
					</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                        <div class="header-linked">
                              <a href="" blank class="link-shop link-shoppe"><img src="<?=base_url()?>img/shoppe.png" alt=""></a>
                              <a href="" blank class="link-shop link-lazada"><img src="<?=base_url()?>img/lazada.png" alt=""></a>
                              <a href="" blank class="cart">
                                    <strong>Giỏ hàng</strong>
                                    <b class="total" id="header-cart"><?=$cart ? count($cart) : '0';?></b>
                              </a>
                        </div>
                        </div>
                  </div>
                  <?=$menu_scroll;?>
            </div>
      </div>
</header>
