<div class="news">
    <div class="container">
      <h2>BLOG</h2>
      <div class="row">

    <?php foreach($news as $n) : ?>

        <div class="col-md-3 col-sm-3 col-xs-6 margin20">
          <div class="news-content">
            <div class="new-img">
              <a href="<?= base_url($n->alias.'.html') ?>" title=""><img src="<?= base_url($n->image) ?>" alt="<?= ($n->title); ?>"></a>
            </div>
            <div class="news-description">
              <a href="<?= base_url($n->alias.'.html') ?>" title="" class="news-des-name"><?= ($n->title); ?></a>
              <div class="news-date">
                <i class="date-time fa fa-calendar"> <?=date('d/m/Y', $n->time)?></i>
                
                
              </div>
              <div class="news-content"><?= strip_tags(LimitString($n->description,320,'...'),'<p>');?> </div>
              
            </div>
          </div>
        </div>

      <?php endforeach;?>



      </div>

    </div>
  </div>  