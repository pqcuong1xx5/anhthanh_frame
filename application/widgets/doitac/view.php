<?php if(isset($doitacs)) { ?>
<div class="partner">
    <h2 class="partner-title"><strong>Thương hiệu nổi bật</strong></h2>
    <div class="partner-slider">
		<?php foreach($doitacs as $doitac) :  ?>
        <div class="partner-item">
            <a href="<?=base_url(@$doitac->alias)?>" class="partner-link"><img src="<?=base_url(@$doitac->image)?>" alt=""></a>
        </div>
		<?php endforeach;?>
    </div>
</div>
<?php } ?>