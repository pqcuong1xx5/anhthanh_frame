
<?php if(isset($product_sale)){ ?>
<div class="product-promotion">
	<div class="container">
		<div class="promotion-content">
			<h2 class="promotion-title"><strong>Khuyến mãi - <span>hot deal</span></strong></h2>
			<div class="prommotion-product">
				<div class="row marginrow10 prommotion-product-slider">
					<?php foreach ($product_sale as $key => $pros) { ?>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10">
						<div class="product-item1">
							<a href="<?= base_url('san-pham/'.$pros->alias)?>" class="product-link-img"><img src="<?= base_url('upload/img/products/'.$pros->pro_dir.'/'.$pros->image) ?>" alt=""></a>
							<div class="product-info">
								<a href="<?= base_url('san-pham/'.$pros->alias)?>" class="product-link-name"><?=$pros->name;?></a>
								<div class="product-cart">
									<div class="product-electronic-rating rating-demo">
										<?php if (isset($pros->rating)) { ?>
												<?php if(@$pros->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pros->rating) { ?>
															<?php if( $i == floor(@$pros->rating) &&  @$pros->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
									<a onclick="addcart(<?=$pros->id?>)" class="btn"><i class="fas fa-cart-plus"></i> </a>
								</div>
								<p class="product-price"><?php if($pros->price_sale >0 ) {?><?=number_format($pros->price_sale);?> đ<?php }else{ echo "Liên hệ";} ?></p>
								<?php if($pros->price >0 ) {?>
									<p class="product-price-sale"><?=number_format($pros->price);?></p>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
