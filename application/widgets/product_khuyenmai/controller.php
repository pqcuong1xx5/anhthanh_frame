<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_khuyenmai_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){
        
		//$this->load->model('f_homemodel');
		
		//san pham mới	
		$data['product_sale'] = $this->system_model->get_data('product',array(
            'lang' => $this->language,
            'home'=>1
		),array('sort' => 'desc'),null,30,0);
		if(isset($data['product_sale'])){
			foreach($data['product_sale'] as $k => $v){
				$data['array_json'] = json_decode($v->config_pro);
				if(!empty($data['array_json'])){
					foreach ($data['array_json'] as $keyJson => $json) {
						if (isset($json->name) && strpos($json->name,"Vope star")!==false) {
							 $data['product_sale'][$k]->rating =  $json->content;
						}
						if (isset($json->name) && strpos($json->name,"Chi tiết sản phẩm") !== false) {
							 $data['product_sale'][$k]->content =  $json->content;
						}
					}
				}
			}
		}
	
	    $this->load->view('view',$data);
    }
}
