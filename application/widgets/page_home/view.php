<article id="body_home">
 <!-- info -->
    <div class="clearfix-40"></div>
    <div class="sc_info">
        <div class="container">
            <div class="row_pc">

                <div class="clearfix-20"></div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <img src="img/gioithieu.png" class="w_100" alt="">
                        <div class="clearfix-30"></div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="wpb_column vc_column_container vc_col-sm-6       ">
                       <div class="vc_column-inner vc_custom_1542172909008">
                          <div class="wpb_wrapper">
                             <h2 class="vc_custom_heading vc_custom_1542174378097">GIỚI THIỆU</h2>
                             <div id="tb-5c860245b7e6b" class="li-textblock vc_custom_1542176652717">
                                <p><span style="color: #000000;">Thiết bị  vệ sinh thông minh LUVA BIDET được nhập khẩu từ NhẬT BẢN. Đạt tiêu chuẩn về chất lượng, vệ sinh do bộ y tế NHẬT BẢN cấp. 90% dân số Nhật Bản dùng thiết bị vệ sinh thông minh, thay thế cho giấy về sinh truyền thống mang lại lợi ích về sức khỏe và kinh tế không nhỏ cho các thành viên gia đình bạn.<br>
                                   </span>

                                </p>
                             </div>
                          </div>
                       </div>
                      </div>
                    </div>
                </div>
                <div class="clearfix-10"></div>
                <div class="tit_main text-center">
                    <img src="img/Home_09.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix-40"></div>
<!-- end info -->
</article>