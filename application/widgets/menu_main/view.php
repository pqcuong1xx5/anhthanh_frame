
<?php if(count($menu_root)) : ?>
<div class="menu-start">
	<div class="container">
		<div class="menu-list">
			<ul class="menu-parent" id="menu-top-parent">
				
					<li class="main-closes">
					<span>×</span>
					</li>

					<?php foreach ($menu_root as $key_r => $mr) : ?>
					<li class="parent-item">
						<a href="<?=base_url($mr->url);?>" title="<?=@$mr->name;?>" class="menu-link"><?=@$mr->name;?></a>
						<?php if(!empty($mr->menu_sub)): ?>
						<ul class="menu-child">
							<?php $i=0; foreach($mr->menu_sub as $menu_sub) : $i++; ?>
							<li class="child-item">
									<a href="<?=base_url($menu_sub->url);?>" title="<?=@$menu_sub->name;?>" class="menu-link"><?=@$menu_sub->name;?></a>
							</li>
							<?php endforeach;?>
						</ul>
						<?php endif;?>
					</li>
					<?php endforeach;?>
					
			</ul>
		</div>
	</div>
</div>
<?php endif;?>