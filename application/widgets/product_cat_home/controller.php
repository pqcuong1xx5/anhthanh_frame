<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_cat_home_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){

        $data = array();
		$data['cate_hot'] = $this->system_model->get_data('product_category',array(
            'lang' => $this->language,
            'parent_id'=>0,
            'focus'=>1
        ),array('sort' => 'desc'),null,1,0);
        if(isset($data['cate_hot'])){
            foreach ($data['cate_hot'] as $k => $cat) {
				$data['cate_hot'][$k]->cat_child = $this->system_model->get_data('product_category',array(
					'lang' => $this->language,
					'parent_id'=>$cat->id,
                ),array('sort' => 'desc'),null,3,0);
                
				if(isset($data['cate_hot'][$k]->cat_child)){
					foreach ($data['cate_hot'][$k]->cat_child as $key => $item_child) {


                        $data['all_pros'] = $this->f_homemodel->get_products(array(
							'lang' => $this->language,
							'product_to_category.id_category'=> $item_child->id,
                             ),array('sort' => 'desc'),25,0);
                        if(isset( $data['all_pros'])){
                            foreach ( $data['all_pros'] as $keyPro => $value) {
                                $data['array_json'] = json_decode($value->config_pro);
                                if(!empty($data['array_json'])){
                                    foreach ($data['array_json'] as $keyJson => $json) {
                                        if (isset($json->name) && strpos($json->name,"Vope star")!==false) {
											$data['all_pros'][$keyPro]->rating =  $json->content;
									   }
									   if (isset($json->name) && strpos($json->name,"Chi tiết sản phẩm") !== false) {
											$data['all_pros'][$keyPro]->content =  $json->content;
									   }
                                    }
                                }
                            }
                        
                        }
                        $data['pros']=[];
                        if(isset($data['all_pros']) && count($data['all_pros'])!=0){
                            $count = count($data['all_pros']);
                            $i=-1;
                            $j = 0;
                            while($count > 0){
                                
                                $data['pros']['column_'.$j] = array();
                                if(isset( $data['all_pros'][$i+1])){
                                    array_push($data['pros']['column_'.$j], $data['all_pros'][$i+1]);
                                }
                                if(isset( $data['all_pros'][$i+2])){
                                    array_push($data['pros']['column_'.$j], $data['all_pros'][$i+2]);
                                }
                                $j++;
                                $i+=2;
                                $count-=2;
                                
                            }
                        }
                        
                        $data['cate_hot'][$k]->cat_child[$key]->pro_cate_child = $data['pros'];
						
					}
				}
            }   
        }
 
        $this->load->view('view',$data);
    }

}
