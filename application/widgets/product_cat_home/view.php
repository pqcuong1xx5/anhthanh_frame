
<?php if(isset($cate_hot)) { $i=0; ?>
<?php foreach ($cate_hot as $k => $cate_parent) { ?>
<div class="category-object">
	<div class="container">
		<div class="category-bar">
			<h3 class="category-title"><a href="<?=base_url($cate_parent->alias);?>"><?=$cate_parent->name;?></a></h3>
			<ul class="category-tab">
				<?php if(isset($cate_parent->cat_child)) { ?>
				<?php foreach ($cate_parent->cat_child as $k => $cate) { $i++; ?>
					<li class="category-item <?php if($i==1){ echo 'tab-active' ;}?>"><?=$cate->name;?></li>
				<?php } ?>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="product-object">
		<div class="container">
			<div class="pro-object-content">
				<div class="row marginrow10 pro-object-slider">
				<?php if(isset($cate_parent->cat_child)) { ?>
					<?php foreach ($cate_parent->cat_child as $k => $cate) { ?>
					<?php if(isset($cate->pro_cate_child)) { ?>
					<?php foreach ($cate->pro_cate_child as $k => $pro) { ?>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
						<?php if(isset($pro)) {  ?>
						<?php foreach ($pro as $k => $pro) { $i++; ?>
						<div class="product-item1 margin10">
							<a href="<?=base_url('san-pham/'.$pro->alias);?>" class="product-link-img"><img src="<?= base_url('upload/img/products/'.$pro->pro_dir.'/'.$pro->image) ?>" alt=""></a>
							<div class="product-info">
								<a href="<?=base_url('san-pham/'.$pro->alias);?>" class="product-link-name"><?=$pro->name;?></a>
								<div class="product-cart">
									<div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
									<a href="" class="btn"><i class="fas fa-cart-plus"></i> </a>
								</div>
								<p class="product-price"><?php if($pro->price_sale >0 ) {?><?=number_format($pro->price_sale);?> đ<?php }else{ echo "Liên hệ";} ?></p>
								<?php if($pro->price >0 ) {?>
									<p class="product-price-sale"><?=number_format($pro->price);?></p>
								<?php } ?>
							</div>
						</div>
						<?php }?>
						<?php }?>
						
					</div>
					<?php }?>
					<?php }?>
					<?php } ?>
				<?php } ?>
					
				</div>

			</div>
		</div>
	</div>

</div>
<?php } ?>
<?php } ?>
