
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KEY</title>


    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.css" rel="stylesheet"/>
    <link href="css/owl.carousel.css" rel="stylesheet"/>
    <link href="css/common.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>

</head>
<body>
<!-- main content -->
<main>
  <div class="container">
    <div class="row_pc">
      <div class="owl-carousel slider_main">
            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/hoang_phi_cau_doi_dep.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Hoàng phi câu đối đẹp</h2>
                </a>
            </div>
            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/thucuon.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Cuốn thư</h2>
                </a>
            </div>
            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/sap_tho.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Sập thờ</h2>
                </a>
            </div>

            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/ban_o_sa.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Bàn ô sa <br />Bàn án gian</h2>
                </a>
            </div>
            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/cua_vong.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Cửa võng</h2>
                </a>
            </div>

            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/noi_that_khong_gian_tho.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Nội thất không gian thờ</h2>
                </a>
            </div>
            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/tuong_phat_son_dong.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Tượng phật sơn đồng</h2>
                </a>
            </div>
            <div class="item">
                <a href="#" title="" class="dotho_item">
                  <img src="img/kham_tho.png" alt="" class="img_dotho">
                  <h2 class="name_dotho">Khám thờ</h2>
                </a>
            </div>

        </div>
    </div>
  </div>
  <script type="text/javascript">
     $(function() {
        $(".slider_main").owlCarousel({
        items: 8,
        responsive: {
            1200: { item: 8, },// breakpoint from 1200 up
            992: { items: 7, },
            768: { items: 6, },
            480: { items: 5, },
            0: { items: 3, }
        },
        rewind: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000, //slide speed smooth
        dots: false,
        dotsEach: false,
        loop: true,
        nav: true,
        navText: ['<i class="fa fa-angle-left arrow-slider"></i>','<i class="fa fa-angle-right arrow-slider"></i>'],
        margin: 62,
        animateOut: ['fadeOutUp', 'zoomOut', 'fadeOutLeft'], // default: false
        animateIn: ['fadeInDown', 'zoomIn','fadeInLeft'], // default: false
        center: false,
    });
});

  </script>
</main>

</body>
</html>
