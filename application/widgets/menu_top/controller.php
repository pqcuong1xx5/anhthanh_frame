<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_top_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){

		//menu top
		$data['menu_category'] = $this->f_homemodel->get_data('menu',array(
			'position'=>'top',
			'parent_id'=>0,
			'lang' => $this->language
			),array('sort'=>''));
		if(isset($data['menu_category'])){
			foreach ($data['menu_category'] as $key1 => $cat1) {
				$data['menu_category'][$key1]->menu_level2 =  $this->system_model->get_data('menu',array( 
				'parent_id ='=>$cat1->id,
				'lang' => $this->language),
					array('sort'=>''));
				if(isset($data['menu_category'][$key1]->menu_level2)){
					foreach($data['menu_category'][$key1]->menu_level2 as $key2=>$cat2){
						$data['menu_category'][$key1]->menu_level2[$key2]->menu_level3 = $this->system_model->get_data('menu',array( 
							'parent_id ='=>$cat2->id,
							'lang' => $this->language),
								array('sort'=>''));
						if(isset($data['menu_category'][$key1]->menu_level2[$key2]->menu_level3)){
							foreach($data['menu_category'][$key1]->menu_level2[$key2]->menu_level3 as $key3=>$cat3){
								$data['menu_category'][$key1]->menu_level2[$key2]->menu_level3[$key3]->menu_level4 = $this->system_model->get_data('menu',array( 
									'parent_id ='=>$cat3->id,
									'lang' => $this->language),
										array('sort'=>''));
							}
						}
					}
				}
			}
		}

		$this->load->view('view',$data);
    }
}
