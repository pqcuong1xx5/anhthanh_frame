<?php if(isset($menu_bottom)) : ?>
<?php foreach ($menu_bottom as $key_r => $mr) : ?>
<div class="col-md-2 col-sm-2 col-xs-2 col-footer">
	<div class="footer-service">
		<h3 class="service-title"><?= @$mr->name; ?></h3>
		<?php if (isset($mr->menu_sub)): ?>
		<ul class="list-service">
			<?php foreach ($mr->menu_sub as $key => $menu_sub) { ?>
			<li><a href="<?=base_url($menu_sub->alias); ?>" class="service-link"><?= @$menu_sub->name; ?></a></li>
			<?php }?>
		</ul>
		<?php endif ?> 
	</div>
</div>
<?php endforeach;?>
<?php endif;?>
