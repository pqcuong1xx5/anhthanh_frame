<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_right_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){
        
		$this->load->model('f_homemodel');
		
		//menu left
		
		$data['menu_category'] = $this->f_homemodel->get_data('menu',array(
			'position'=>'top',
			'parent_id'=>0,
			'lang' => $this->language
			),array('sort'=>''));
		   if(isset($data['menu_category'])){
			foreach ($data['menu_category'] as $key1 => $cat1) {
				$data['menu_category'][$key1]->menu_level2 =  $this->system_model->get_data('menu',array( 
				'parent_id ='=>$cat1->id,
				'lang' => $this->language),
					array('sort'=>''));
				if(isset($data['menu_category'][$key1]->menu_level2)){
					foreach($data['menu_category'][$key1]->menu_level2 as $key2=>$cat2){
						$data['menu_category'][$key1]->menu_level2[$key2]->menu_level3 = $this->system_model->get_data('menu',array( 
							'parent_id ='=>$cat2->id,
							'lang' => $this->language),
								array('sort'=>''));
						if(isset($data['menu_category'][$key1]->menu_level2[$key2]->menu_level3)){
							foreach($data['menu_category'][$key1]->menu_level2[$key2]->menu_level3 as $key3=>$cat3){
								$data['menu_category'][$key1]->menu_level2[$key2]->menu_level3[$key3]->menu_level4 = $this->system_model->get_data('menu',array( 
									'parent_id ='=>$cat3->id,
									'lang' => $this->language),
										array('sort'=>''));
							}
						}
					}
				}
			}
		}
		// foreach ($data['menu_root'] as $key => $cat) {
			// $data['menu_root'][$key]->menu_sub =  $this->f_homemodel->get_data('menu',array( 'position'=>'bottom2',
			// 'parent_id ='=>$cat->id_menu,
			// 'lang' => $this->language),
                // array('sort'=>''));
		// }		

			$this->load->view('view',$data);
    }
}
