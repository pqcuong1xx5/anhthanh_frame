
	<div class="menu-category">
		<p class="clearfix btn-open-category bold600"><i>☰</i><span>DANH MỤC SẢN PHẨM</span></p>
		<?php if(isset($menu_category)) : ?>
		<ul class="menu-category-parent">
			<?php foreach ($menu_category as $key1 => $menu1) : ?>
			<li class="category-parent-item">
				<a href="<?=base_url($menu1->url);?>" class="category-link" title="<?=$menu1->name;?>">
					<span class="category-img">
						<img src="img/cat1.png" alt="">
					</span>
					<span class="category-name"><?=$menu1->name;?></span>
				</a>
				<?php if(isset($menu1->menu_level2)){ ?>
				<ul class="scroll-child2">
					<?php foreach ($menu1->menu_level2 as $key2 => $menu2) {?>
					<li class="child2-item">
						<a href="<?=base_url($menu2->url);?>" title="<?=$menu2->name;?>" class="child2-link"><?=$menu2->name;?></a>
						<?php if(isset($menu2->menu_level3)){ ?>
						<ul class="scroll-child3">
							<?php foreach ($menu2->menu_level3 as $key3 => $menu3) {?>
							<li class="child3-item">
								<a class="child3-link" href="<?=base_url($menu3->url);?>" title="<?=$menu3->name;?>"><?=$menu3->name;?></a>
								<?php if(isset($menu3->menu_level4)){ ?>
								<ul class="scroll-child4">
									<?php foreach ($menu3->menu_level4 as $key4 => $menu4) {?>
									<li class="child4-item"><a class="child4-link" href="<?=base_url($menu4->url);?>" title="<?=$menu4->name;?>"><?=$menu4->name;?></a></li>
									<?php } ?>
									
								</ul>
								<?php } ?>
							</li>
							<?php } ?>
							
						</ul>
						<?php } ?>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php endforeach;?>
		</ul>
		<?php endif;?>
	</div>
			 