<div class="product-top">
    <div class="container">
        <div class="product-top-content">
            <div class="top-tab">
                <ul class="top-tap-list">
                    <li class="top-tap-item active"  onclick="getProductNew('hot')">Sản phẩm mới</li>
                    <li class="top-tap-item" onclick="getProductNew('focus')">Bán chạy</li>
                    <li class="top-tap-item" onclick="getProductNew('coupon')">Cao cấp</li>
                </ul>
            </div>

        </div>
    </div>
    <?php if(isset($pros)){?>
    <div class="top-product">
		<section class=" loading-component" id="top-pro-loading">
			<div class="loader loader-2">
			<svg class="loader-star" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
					<polygon points="29.8 0.3 22.8 21.8 0 21.8 18.5 35.2 11.5 56.7 29.8 43.4 48.2 56.7 41.2 35.1 59.6 21.8 36.8 21.8 " fill="#18ffff" />
				</svg>
			<div class="loader-circles"></div>
			</div>
		</section>
        <div class="container">
		
            <div class="top-product-list" id="product_top">
                <div class="row marginrow10 top-product-slider">
                    <?php foreach($pros as $k => $pro){ ?>
                    <div class="col-sm-2dot4" style="max-width: auto;">
                        <div class="product-item1">
                            <a href="<?= base_url('san-pham/'.$pro->alias)?>" class="product-link-img"><img src="<?= base_url('upload/img/products/'.$pro->pro_dir.'/'.$pro->image) ?>" alt=""></a>
                            <div class="product-info">
                                <a href="<?= base_url('san-pham/'.$pro->alias)?>" class="product-link-name"><?=$pro->name;?></a>
								<div class="product-cart">
									<div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
									<a href="" class="btn"><i class="fas fa-cart-plus"></i> </a>
								</div>
                                <p class="product-price"><?php if($pro->price_sale >0 ) {?><?=number_format($pro->price_sale);?> đ<?php }else{ echo "Liên hệ";} ?></p>
                                <?php if($pro->price >0 ) {?>
									<p class="product-price-sale"><?=number_format($pro->price);?></p>
								<?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
