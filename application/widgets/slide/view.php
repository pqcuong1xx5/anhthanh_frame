<?php if(isset($category_slide)) { ?>
<div class="top-category">
	<div class="container-ics">
		<div class="top-category-slider">
			<?php foreach($category_slide as $k => $v){ ?>
				<div class="category-item">
					<a href="<?= base_url('danh-muc/'.$v->alias)?>" title="" class="category-item-link"><img src="<?= base_url($v->image)?>" alt=""></a>
					<a href="<?= base_url('danh-muc/'.$v->alias)?>" title="" class="category-item-title"><?=$v->name?></a>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php } ?>
