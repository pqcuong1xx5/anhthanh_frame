<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slide_widget extends MY_Widget
{
    // Nhận 2 biến truyền vào
    function index(){

		$data = array();

		$data['category_slide'] = $this->system_model->get_data('product_category',array(
            //'home' => 1,
            'lang' => $this->language,
            'parent_id' => 0,
            'focus' => 1
            ),array('sort' => 'desc'));
        $this->load->view('view',$data);
    }
    
}
