<div class="category-object  category-case">
        <div class="container">
            <div class="category-bar">
                <h3 class="category-title"><a href="">Màn hình máy tính</a></h3>
                <ul class="category-tab">
                    <li class="category-item tab-active">Văn phòng</li>
                    <li class="category-item">Gaming</li>
                    <li class="category-item">Đồ họa</li>
                </ul>
            </div>
        </div>
        <div class="product-object">
            <div class="container">
                <div class="pro-object-content">
                    <div class="row marginrow10 pro-object-slider">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
									<div class="product-cart">
										<div class="product-electronic-rating rating-demo">
											<?php if (isset($pro->rating)) { ?>
													<?php if(@$pro->rating > 0){ ?>
														<?php for( $i=0; $i<5; $i++ ){ ?>
															<?php if( $i <= @$pro->rating) { ?>
																<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																	<span class="fas fa-star-half-alt"></span>
																<?php }else{ ?>
																	<span class="fas fa-star checked"></span>
																<?php } ?>
															<?php }else{ ?>
																<span class="far fa-star"></span>
															<?php } ?>
														<?php } ?>
													<?php }else{ ?>
														<div class="no-rating">
															<span class="far fa-star"></span>
															<span class="far fa-star"></span>
															<span class="far fa-star"></span>
															<span class="far fa-star" aria-hidden="true"></span>
															<span class="far fa-star" aria-hidden="true"></span>
														</div>
													<?php } ?>
											<?php  }else{?>
												<div class="no-rating">
													<span class="far fa-star"></span>
													<span class="far fa-star"></span>
													<span class="far fa-star"></span>
													<span class="far fa-star" aria-hidden="true"></span>
													<span class="far fa-star" aria-hidden="true"></span>
												</div>
											<?php } ?>
										</div>
										<a href="" class="btn"><i class="fas fa-cart-plus"></i> </a>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                    <p class="product-price-sale">120.000d</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                    <p class="product-price-sale">120.000d</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                    <p class="product-price-sale">120.000d</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 paddingcol10  ">
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                            <div class="product-item1 margin10">
                                <a href="" class="product-link-img"><img src="img/promo1.jpg" alt=""></a>
                                <div class="product-info">
                                    <a href="" class="product-link-name">EliteBook X360 1040 G5/ i7-8565U-1.8G/ 16G/ 512G SSD/ 14 FHD+Touch,Pen/ W10P/ Silver</a>
                                    <div class="product-electronic-rating rating-demo">
										<?php if (isset($pro->rating)) { ?>
												<?php if(@$pro->rating > 0){ ?>
													<?php for( $i=0; $i<5; $i++ ){ ?>
														<?php if( $i <= @$pro->rating) { ?>
															<?php if( $i == floor(@$pro->rating) &&  @$pro->rating-$i !=0 ) { ?>
																<span class="fas fa-star-half-alt"></span>
															<?php }else{ ?>
																<span class="fas fa-star checked"></span>
															<?php } ?>
														<?php }else{ ?>
															<span class="far fa-star"></span>
														<?php } ?>
													<?php } ?>
												<?php }else{ ?>
													<div class="no-rating">
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star"></span>
														<span class="far fa-star" aria-hidden="true"></span>
														<span class="far fa-star" aria-hidden="true"></span>
													</div>
												<?php } ?>
										<?php  }else{?>
											<div class="no-rating">
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star"></span>
												<span class="far fa-star" aria-hidden="true"></span>
												<span class="far fa-star" aria-hidden="true"></span>
											</div>
										<?php } ?>
									</div>
                                    <p class="product-price">Liên hệ</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
