$(document).ready(function(){
	$('.hd-btnmenu').click(function(){
		$('.hd-btnmenu i').toggleClass('fa-bars').toggleClass('fa-times');
		$('.col-hd-menu').toggleClass('active');
		$('.hd-menu ul li a i').toggleClass('fa-chevron-right').toggleClass('fa-caret-down');
	});

});

$(document).ready(function () {
	$('.banner-slider').slick({

		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		autoplay: true,
		autoplaySpeed: 5000
		// responsive: [
		// {
		// 	breakpoint: 1024,
		// 	settings: {
		// 		slidesToShow: 3,
		// 		slidesToScroll: 1,
		// 		infinite: true,
		// 		dots: true
		// 	}
		// },
		// {
		// 	breakpoint: 600,
		// 	settings: {
		// 		slidesToShow: 2,
		// 		slidesToScroll: 1
		// 	}
		// },
		// {
		// 	breakpoint: 480,
		// 	settings: {
		// 		slidesToShow: 1,
		// 		slidesToScroll: 1
		// 	}
		// }
		// ]
	});
});
$(document).ready(function(){
	$('.plus').click(function(){
		var parent = $(this).parent();
		var input = parent.find('.input-number')
		var val = parseInt(input.val());
		val +=1;
		input.val(val);
	})

	$('.minus').click(function(){
		var parent = $(this).parent();
		var input = parent.find('.input-number');
		var val= parseInt(input.val());
		if(val<1){
			input.val(val);
		} else{
			val -=1;
			input.val(val);
		}
	});
	$.fn.numberOnly = function(){
		return this.each(function(){
			$(this).keydown(function(e){
				var key = e.charCode||e.keyCode||0;
				return(
					key == 8 || 
					key == 9 ||
					key == 13 ||
					key == 46 ||
                    // key == 110 ||
                    // key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));

			})
		})
	}
	$(".input-number").numberOnly();
});
$(document).ready(function(){
	$('.plus1').click(function(){
		var parent = $(this).parent();
		var input = parent.find('.input-book')
		var val = parseInt(input.val());
		val +=1;
		input.val(val);
	})

	$('.minus1').click(function(){
		var parent = $(this).parent();
		var input = parent.find('.input-book');
		var val= parseInt(input.val());
		if(val<1){
			input.val(val);
		} else{
			val -=1;
			input.val(val);
		}
	});
	$.fn.numberOnly = function(){
		return this.each(function(){
			$(this).keydown(function(e){
				var key = e.charCode||e.keyCode||0;
				return(
					key == 8 || 
					key == 9 ||
					key == 13 ||
					key == 46 ||
                    // key == 110 ||
                    // key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));

			})
		})
	}
	$(".input-book").numberOnly();
});
$(document).ready(function(){
	$('.fancy').fancybox({
		loop: true,
		buttons: [
		"zoom",
		"share",
		"slideShow",
		"fullScreen",
		"download",
		"thumbs",
		"close"
		],
	});

});

$(document).ready(function(){
	$('.prd-img').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.prd-slimg1,.prd-slimg2'
	});
	$('.prd-slimg1').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		focusOnSelect: true,
		asNavFor: '.prd-img, .prd-slimg2'
	});
	$('.prd-slimg2').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		focusOnSelect: true,
		asNavFor: '.prd-img, .prd-slimg1',

		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				infinite: true,
				dots: true
			}

		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 3,
				sliderToScroll: 1,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				sliderToScroll: 1
			}
		}
		]
	});
});