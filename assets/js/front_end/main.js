$(document).ready(function() {
    setTimeout(function() {
        $('#section-loading').removeClass('loading');
    }, 3000);

    $(window).scroll(function() {
        var aTop = $('#header').height();
        if ($(this).scrollTop() >= aTop) {
            $('#header').addClass('header-active');
        } else {
            $('#header').removeClass('header-active');
            $('#menu-scroll').removeClass('active');
        }
    });
    $('.btn-menu').click(function() {
        document.getElementById('menu-top-parent').classList.add('active');
    });
    $('.top-tap-item').click(function() {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });
    $('.main-closes').click(function() {
        document.getElementById('menu-top-parent').classList.remove('active');
    });
    $('.btn-show-content').click(function() {
        document.getElementById('product-content').classList.add('active');
    });
    $('.btn-hidden-content').click(function() {
        document.getElementById('product-content').classList.remove('active');
    });
    $('.open-menu-scroll').click(function() {
        var classlist = document.getElementById('menu-scroll').classList;
        if (classlist.contains('active')) {
            document.getElementById('menu-scroll').classList.remove('active');
        } else {
            document.getElementById('menu-scroll').classList.add('active');
        }
    });

    $('.banner-slider').slick({
        infinit: true,
        dots: true,
        nextArrow: '<button class="btn-slider btn-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-slider btn-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
    });
    $('.partner-slider').slick({
        infinit: true,
        dots: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-partner partner-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-partner partner-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 5,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.prommotion-product-slider').slick({
        infinit: true,
        dots: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-sliders btn-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-sliders btn-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 5,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.top-product-slider').slick({
        infinit: true,
        dots: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-sliders btn-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-sliders btn-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
        responsive: [{
                breakpoint: 1600,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.pro-object-slider').slick({
        infinit: true,
        dots: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-category category-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-category category-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
        responsive: [{
                breakpoint: 1600,
                settings: {
                    slidesToShow: 5,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.similar-slider').slick({
        infinit: true,
        dots: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-category category-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-category category-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',
        responsive: [{
                breakpoint: 1600,
                settings: {
                    slidesToShow: 5,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.advertisement-slider').slick({
        infinit: true,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: '<button class="btn-sliders btn-next"><i class="fa fa-chevron-right " aria-hidden="true"></i></button>',
        prevArrow: '<button class="btn-sliders btn-prev"><i class="fa fa-chevron-left " aria-hidden="true"></i></button>',

    });
    $('#scrollToTop').click(function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        verticalSwiping: true,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        focusOnSelect: true,
        nextArrow: '<button class="btn-sliders btn-next"></button>',
        prevArrow: '<button class="btn-sliders btn-prev"></button>',
    });

    // ZOOM
    $('.ex1').zoom();

    // STYLE GRAB
    $('.ex2').zoom({ on: 'grab' });

    // STYLE CLICK
    $('.ex3').zoom({ on: 'click' });

    // STYLE TOGGLE
    $('.ex4').zoom({ on: 'toggle' });

});